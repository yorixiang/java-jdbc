package com.tgl.demo;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://10.67.67.186:3306/emp_yori?characterEncoding=utf8";
	
	static final String USER = "root";
	static final String PASS = "123456";

	public static void main(String[] args) {
		  DBConnection objDBConnection=new DBConnection();
		  System.out.println(objDBConnection.getConnection());
	}
	
	public Connection getConnection() {
		 Connection connection=null;
		try {
		  Class.forName(JDBC_DRIVER);              
		  connection = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (Exception e) {
			System.err.println("didn't connection");
		} 
		  return connection;
	}

}
