package com.tgl.demo;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CRUDdemo {

	public static void main(String[] args) {
		
		CRUDdemo demoObject = new CRUDdemo();
//		demoObject.createData();
//		demoObject.updateData();
//		demoObject.deleteData();
		demoObject.readData(1);

	}
	
	public void createData() {
		
		DBConnection DBConnectionObject = new DBConnection();
		Connection connection = DBConnectionObject.getConnection();
		PreparedStatement preparedstatement = null;
		
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		java.text.SimpleDateFormat SDF = 
		     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			
			String create = "INSERT INTO staffTable (staffHeight, staffWeight, "
					+ "staffEnglishName, staffChineseName, "
					+ "staffExt, staffEmail, staffBMI, "
					+ "createDateTime, updateDateTime)"
					+ " VALUES (?,?,?,?,?,?,?,?,?)";
			
			preparedstatement = connection.prepareStatement(create);
			preparedstatement.setInt(1, 1);
			preparedstatement.setInt(2, 1);
			preparedstatement.setString(3, "a");
			preparedstatement.setString(4, "a");
			preparedstatement.setString(5, "a");
			preparedstatement.setString(6, "a");
			preparedstatement.setDouble(7, 1.00d);
			preparedstatement.setString(8, SDF.format(date));
			preparedstatement.setString(9, SDF.format(timestamp));
			preparedstatement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (preparedstatement != null) preparedstatement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<Staff> readData(int staffID) {
		
		DBConnection DBConnectionObject = new DBConnection();
		Connection connection = DBConnectionObject.getConnection();
		PreparedStatement preparedstatement = null;
		ResultSet resultset = null;
		
		List<Staff> staffArrayList = new ArrayList<>();
		
		try {
			
			String read = "SELECT * FROM staffTable WHERE staffID = ?";
			preparedstatement = connection.prepareStatement(read);
			preparedstatement.setInt(1, staffID);
			resultset = preparedstatement.executeQuery();
			
			while(resultset.next()) {
				
				Staff aStaff = new Staff(resultset.getString("staffHeight"),
						resultset.getString("staffWeight"),
						resultset.getString("staffEnglishName"),
						resultset.getString("staffChineseNmae"),
						resultset.getString("staffExt"),
						resultset.getString("staffEmail"),
						resultset.getString("staffBMI"));
				
				Record record = new Record();
				record.setCreateTime(resultset.getString("createDateTime"));
				record.setCreateTime(resultset.getString("updateDateTime"));
				aStaff.setRecord(record);
						
				staffArrayList.add(aStaff);
				
				char[] ChineseName = resultset.getString("staffChineseName").toCharArray();
				if (ChineseName.length == 2 || ChineseName.length == 3) {
					ChineseName[1] = 'X';
				} else if (ChineseName.length == 4) {
					ChineseName[1] = 'X';
					ChineseName[2] = 'X';
				}
				
//				Staff staff = null;
//				Record record = new Record();
//				record.setCreateTime("");
//				record.setUpdateTime("");
//				staff.setRecord(record);

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultset != null) resultset.close();
				if (preparedstatement != null) preparedstatement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return staffArrayList;
		
	}
	
	public void readData(String schemaColumn, int minimum, String ogicalOperator, int maximum) {
		
		DBConnection DBConnectionObject = new DBConnection();
		Connection connection = DBConnectionObject.getConnection();
		PreparedStatement preparedstatement = null;
		ResultSet resultset = null;
		
		try {
			
			String read = "SELECT * FROM staffTable ORDER BY " + schemaColumn + " desc";
			preparedstatement = connection.prepareStatement(read);
			resultset = preparedstatement.executeQuery();
			
			while(resultset.next()) {

				char[] ChineseName = resultset.getString("staffChineseName").toCharArray();
				if (ChineseName.length == 2 || ChineseName.length == 3) {
					ChineseName[1] = 'X';
				} else if (ChineseName.length == 4) {
					ChineseName[1] = 'X';
					ChineseName[2] = 'X';
				}
				
				System.out.print(resultset.getString("staffID") + " ");
				System.out.print(resultset.getString("staffHeight") + " ");
				System.out.print(resultset.getString("staffWeight") + " ");
				System.out.print(resultset.getString("staffEnglishName") + " ");
				System.out.print(new String(ChineseName) + " ");
				System.out.print(resultset.getString("staffExt") + " ");
				System.out.print(resultset.getString("staffEmail") + " ");
				System.out.print(resultset.getString("staffBMI") + " ");
				System.out.print(resultset.getString("createDateTime") + " ");
				System.out.print(resultset.getString("updateDateTime") + " ");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultset != null) resultset.close();
				if (preparedstatement != null) preparedstatement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateData() {
		
		DBConnection DBConnectionObject = new DBConnection();
		Connection connection = DBConnectionObject.getConnection();
		PreparedStatement preparedstatement = null;
		
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		java.text.SimpleDateFormat SDF = 
		     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			String update = "UPDATE staffTable SET staffHeight=?, staffWeight=?, "
					+ "staffEnglishName=?, staffChineseName=?, "
					+ "staffExt=?, staffEmail=?, staffBMI=?, "
					+ "updateDateTime=? "
					+ " WHERE staffID=?";
			preparedstatement = connection.prepareStatement(update);
			preparedstatement.setInt(1, 2);
			preparedstatement.setInt(2, 2);
			preparedstatement.setString(3, "b");
			preparedstatement.setString(4, "b");
			preparedstatement.setString(5, "b");
			preparedstatement.setString(6, "b");
			preparedstatement.setDouble(7, 1.00d);
			preparedstatement.setString(8, SDF.format(timestamp));
			preparedstatement.setInt(9, 57);
			preparedstatement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (preparedstatement != null) preparedstatement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void deleteData(int staffID) {
		
		DBConnection DBConnectionObject = new DBConnection();
		Connection connection = DBConnectionObject.getConnection();
		PreparedStatement preparedstatement = null;
		
		try {
			String delete = "DELETE FROM staffTable WHERE staffID=?";
			preparedstatement = connection.prepareStatement(delete);
			preparedstatement.setInt(1, staffID);
			preparedstatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (preparedstatement != null) preparedstatement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
