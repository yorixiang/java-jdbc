package com.tgl.demo;

public class Staff {
	
	private String height;
	
	private String weight;
	
	private String englishName;
	
	private String chineseNmae;
	
	private String ext;
	
	private String email;
	
	private String bmi;
	
	private Record record;
	
	public Staff (String height, String weight, String englishName, String chineseName, String ext, String email, String bmi) {
		this.height = height;
		this.weight = weight;
		this.englishName = englishName;
		this.chineseNmae = chineseName;
		this.ext = ext;
		this.email = email;
		this.bmi = bmi;
	}

	public String getStaffHeight() {
		return height;
	}

	public void setStaffHeight(String height) {
		this.height = height;
	}

	public String getStaffWeight() {
		return weight;
	}

	public void setStaffWeight(String weight) {
		this.weight = weight;
	}

	public String getStaffEnglishName() {
		return englishName;
	}

	public void setStaffEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getStaffChineseNmae() {
		return chineseNmae;
	}

	public void setStaffChineseNmae(String chineseNmae) {
		this.chineseNmae = chineseNmae;
	}

	public String getStaffExt() {
		return ext;
	}

	public void setStaffExt(String ext) {
		this.ext = ext;
	}

	public String getStaffEmail() {
		return email;
	}

	public void setStaffEmail(String email) {
		this.email = email;
	}

	public String getStaffBMI() {
		return bmi;
	}

	public void setStaffBMI(String bmi) {
		this.bmi = bmi;
	}
	
	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}

	@Override
	public String toString() {
		return height + " " + weight + " " + englishName + " " + chineseNmae + " " + ext + " " + email + " " + bmi;
	}
	
}
